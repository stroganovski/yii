<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int $group_id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $photo
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Group $group
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     *
     */
    const SCENARIO_CREATE = 'create';

    /**
     *
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * @var
     */
    public $pass;

    /**
     * @var
     */
    public $pass_repeat;

    /**
     * @var
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     *
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @param $username
     *
     * @return User|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['login' => $username]);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['group_id', 'required'],
            ['group_id', 'integer'],
            [
                'group_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Group::className(),
                'targetAttribute' => ['group_id' => 'id']
            ],

            [['login', 'email'], 'required'],
            [['login', 'email'], 'string', 'max' => 255],
            [['login', 'email'], 'unique'],

            //[['pass', 'pass_repeat'], 'safe'],
            ['pass', 'required', 'on' => [self::SCENARIO_CREATE]],
            ['pass', 'string', 'length' => [6, 32]],
            ['pass', 'compare'],

            [
                'image',
                'image',
                'extensions' => 'png, jpg, gif',
                'minWidth' => 100,
                'maxWidth' => 1000,
                'minHeight' => 100,
                'maxHeight' => 1000
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'login' => 'Login',
            'pass' => 'Password',
            'pass_repeat' => 'Password repeat',
            'password' => 'Password',
            'email' => 'E-mail',
            'photo' => 'Photo',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $authKey
     *
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($auth_key)
    {
        return $this->getAuthKey() === $auth_key;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param bool $insert
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->generateAuthKey();
            }

            if (!empty($this->pass)) {
                $this->setPassword($this->pass);
            }

            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();;
    }

    /**
     * @param $password
     *
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            FileHelper::unlink($this->phote);

            return true;
        }

        return false;
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id'])->inverseOf('users');
    }

    /**
     *
     */
    public function uploadImage()
    {
        if ($this->image instanceof UploadedFile) {
            $filename = $this->id . '.' . $this->image->getExtension();
            $filepath = 'uploads/users/' . $filename;

            $this->image->saveAs($filepath);

            $this->photo = $filepath;
            $this->save(false);
        }
    }
}
