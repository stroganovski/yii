<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'group_id')->dropDownList($groups, [
        'prompt' => '-- Select group --',
        'value' => '2'

    ]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true, 'value' => 'test']) ?>

    <?= $form->field($model, 'pass')->passwordInput(['maxlength' => true, 'value' => '123456']) ?>

    <?= $form->field($model, 'pass_repeat')->passwordInput(['maxlength' => true, 'value' => '123456']) ?>

    <?= $form->field($model, 'email')->input('email', ['maxlength' => true, 'value' => 'test@test.loc']) ?>

    <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
