<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%groups}}`.
 */
class m190428_110708_create_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255)->null(),
        ]);

        $this->insertData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%groups}}');
    }

    /**
     * {@inheritdoc}
     */
    private function insertData()
    {
        $columns =['name'];

        $data =[
            ['Admin'], ['Manager'], ['User']
        ];

        $this->batchInsert('{{%groups}}', $columns, $data);
    }
}
