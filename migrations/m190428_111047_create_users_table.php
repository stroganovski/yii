<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m190428_111047_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey()->unsigned(),
            'group_id' => $this->integer()->null()->unsigned(),
            'login' => $this->string(255)->null()->unique(),
            'password' => $this->string(60)->null(),
            'auth_key' => $this->string(32)->null(),
            'email' => $this->string(255)->null()->unique(),
            'photo' => $this->string(255)->null(),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey(
            'users_group_id_foreign',
            'users',
            'group_id',
            'groups',
            'id',
            null,
            'cascade'
        );

        $this->insertData();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }

    /**
     * {@inheritdoc}
     */
    private function insertData()
    {
        $columns = ['group_id', 'login', 'password', 'email'];

        $data = [
            [1, 'admin', md5('admin'), 'admin@example.loc'],
            [2, 'manager', md5('manager'), 'manager@example.loc'],
            [3, 'user', md5('user'), 'user@example.loc'],
        ];

        $this->batchInsert('{{%users}}', $columns, $data);
    }
}
